﻿using System;
using System.Windows.Forms;

namespace TournamentManager
{
    public partial class ShowInfoForm : Form
    {
        private const string NoData = "N/A";
        private readonly Participant _participant;
        public ShowInfoForm(Participant participant)
        {
            InitializeComponent();

            _participant = participant;

            SetParticipantInfo();
        }

        private void SetParticipantInfo()
        {
            Func<string, string> checkData = x => x.Equals(String.Empty) ? NoData : x;

            lblFirstName.Text = checkData(_participant.FirstName);
            lblSurname.Text = checkData(_participant.Surname);
            lblSex.Text = _participant.Sex.ToString();
            lblCategory.Text = _participant.Category.ToString();
            lblBirthDate.Text = _participant.BirthDate.ToShortDateString();
            lblRating.Text = _participant.Rating.ToString();
            lblNationality.Text = checkData(_participant.Nationality);
            lblClub.Text = checkData(_participant.Club);
            lblLicence.Text = checkData(_participant.LicenceNumber);
        }

        private void btnClose_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}