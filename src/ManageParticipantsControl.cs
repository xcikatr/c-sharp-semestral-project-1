﻿using System;
using System.Windows.Forms;

namespace TournamentManager.src
{
    public partial class ManageParticipantsControl : UserControl
    {
        private const string ParticipantError = "No participant selected";
        private const string ErrorCaption = "Error";

        public delegate void ParticipantsModifiedEventDelegate(object sender, ParticipantsModifiedEventArgs args);
        
        public event ParticipantsModifiedEventDelegate ParticipantsModified;

        protected void OnParticipantsModified(ParticipantsModifiedEventArgs e) => 
            ParticipantsModified?.Invoke(this, e);
        
        public ManageParticipantsControl()
        {
            InitializeComponent();
        }

        public void CbAddParticipant(string name)
        {
            cbParticipants.Items.Add(name);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (SelectedItemIsNull(out var selected, ParticipantError))
                return;

            cbParticipants.Items.Remove(selected);

            OnParticipantsModified(new ParticipantsModifiedEventArgs(
                selected.ToString(),
                OperationType.Remove));

            cbParticipants.ResetText();
        }
        
        private void btnShow_Click(object sender, EventArgs e)
        {
            if (SelectedItemIsNull(out var selected, ParticipantError))
                return;

            OnParticipantsModified(new ParticipantsModifiedEventArgs(
                selected.ToString(),
                OperationType.Show));
        }

        #region helperFunctions

        private bool SelectedItemIsNull(out object selected, string errorMessage)
        {
            if ((selected = cbParticipants.SelectedItem) == null)
            {
                MessageBox.Show(errorMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            return false;
        }
        #endregion

    }
}
