﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace TournamentManager
{
    public partial class ManageTournamentForm : Form
    {
        private const string RemoveParticipantMessage = "Participant with this name is already signed in competition";
        private const string MatchBetweenOnePlayerMessage = "Cannot asign score to match agains person himself";
        private const string ErrorCaption = "Error";
        private const int AutoSaveTimeSeconds = 30;
        private const int Second = 1000;
        
        private TournamentData Data { get; set; }
        private readonly string _path;
        
        public ManageTournamentForm(string path)
        {
            InitializeComponent();
            _path = path;
            LoadDataAsync();
            AssignEventHandlers();

            AutoSave();
        }

        private async void LoadDataAsync()
        {
            try
            {
                if (!File.Exists(_path))
                {
                    return;
                }
                var json = await File.ReadAllTextAsync(_path);
                Data = JsonConvert.DeserializeObject<TournamentData>(json);
                if (Data == null)
                {
                    Data = new TournamentData();
                    return;
                }

                FixSuccessorTree();
                LoadParticipantList();
                LoadGroups();
                LoadPlayOff();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Invoke(new Action(() => throw ex));
            }
            
        }

        private void AutoSave()
        {
            var timer = new System.Timers.Timer();
            timer.Interval = AutoSaveTimeSeconds * Second;
            timer.Elapsed += HandleSaveAsync;
            timer.AutoReset = true;
            timer.Start();
        }
        
        private void AssignEventHandlers()
        {
            enterantCntrl.SubmitParticipantClicked += HandleOnSubmitParticipantClicked;
            manageParticipantsControl.ParticipantsModified += HandleOnParticipantModifiedInvoked;
            computeControl.ComputeGroups += HandleOnComputeGroupsClicked;
            computeControl.ComputePlayOff += HandleOnComputePlayOffClicked;
            groupControl.CbGroupItemSelected += HandleOnGroupSelected;
            groupControl.OpponentsSelected += HandleOnGroupOpponentsSelected;
            groupControl.SubmitClicked += HandleOnGroupControlSubmitClicked;
            playOffControl.RoundSelected += HandleOnRoundSelected;
            playOffControl.MatchSelected += HandleOnMatchSelected;
            playOffControl.SubmitPlayOffMatchClicked += HandleOnSubmitPlayOffMatchClicked;
        }

        private async void HandleSaveAsync(object sender, EventArgs e)
        {
            try
            {
                await Save();
            }
            catch (Exception ex)
            {
                Invoke(new Action(() => throw ex));
            }
        }

        private Task Save()
        {
            return Task.Run(() =>
            {
                try
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Formatting = Formatting.Indented;
                    serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    using (StreamWriter sw = new StreamWriter(_path))
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, Data);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    
                    throw;
                }
                
            });
        }
        
        #region eventHandlers
        
        private void HandleOnSubmitParticipantClicked(object sender, ParticipantAddedEventArgs e)
        {
            if (Data.Participants.Any(x =>
                x.FirstName.Equals(enterantCntrl.NewParticipant.FirstName) &&
                x.Surname.Equals(enterantCntrl.NewParticipant.Surname))) 
            {
                MessageBox.Show(RemoveParticipantMessage, ErrorCaption,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Data.Participants.Add(enterantCntrl.NewParticipant);
            enterantCntrl.ResetControl();
            manageParticipantsControl.CbAddParticipant($"{e.Participant.FirstName} {e.Participant.Surname}");
        }
        
        private void HandleOnParticipantModifiedInvoked(object sender, ParticipantsModifiedEventArgs args)
        {
            var name = args.ParticipantName.Split();

            var participant = Data.Participants.FirstOrDefault(x =>
                x.FirstName.Equals(name[0]) && x.Surname.Equals(name[1]));

            if (participant == null)
            {
                return;
            }
            
            switch (args.Type)
            {
                case OperationType.Show :
                    ShowParticipantInfo(participant);
                    break;
                case OperationType.Remove :
                    RemoveParticipant(participant);
                    break;
            }
        }

        private void HandleOnComputeGroupsClicked(object sender, ComputeEventArgs e)
        {
            groupControl.ClearInputData();
            var numberOfGroups = (Data.Participants.Count - 1) / e.Amount + 1;
            var groupIndex = 0;
            var orderedParticipants = Data.Participants.OrderBy(x => x.Rating);

            groupControl.AddGroups(numberOfGroups);
            Data.Groups = new List<Group>();
            for (int i = 0; i < numberOfGroups; i++)
            {
                Data.Groups.Add(new Group());
            }
            
            foreach (var participant in orderedParticipants)
            {
                if (groupIndex >= numberOfGroups)
                {
                    groupIndex = 0;
                }
                Data.Groups[groupIndex].Members.Add(participant);
                groupIndex++;
            }

            foreach (var group in Data.Groups)
            {
                var memberCount = group.Members.Count;
                group.Matches = new int[memberCount, memberCount];
            }
        }
        
        private async void HandleOnComputePlayOffClicked(object sender, ComputeEventArgs e)
        {
            playOffControl.ResetInput();
            try
            {
                var toKeep = (int)Math.Round(((double)Data.Participants.Count / (double)100) * (double)(100 - e.Amount), 0);
                var sortedParticipants = (await ComputeAllGroupOrders()).Take(toKeep).ToList();
                Data.PlayOff = new PlayOffTree(sortedParticipants);
                playOffControl.SetRounds(Data.PlayOff.Depth);
            }
            catch (Exception ex)
            {
                Invoke(new Action(() => throw ex));
            }
        }

        private void HandleOnGroupSelected(object sender, CbItemSelectedEventArgs e)
        {
            if (!int.TryParse(e.ItemName.Split(" ")[1], out var result))
            {
                return;
            }

            var groupMembers = Data.Groups[result - 1].Members
                .Select(x => $"{x.FirstName} {x.Surname}").ToArray();
            groupControl.AddToFirstOpponentList(groupMembers);
            groupControl.AddToSecondOpponentList(groupMembers);
        }
        
        private void HandleOnGroupOpponentsSelected(object sender, OpponentsSelectedEventArgs e)
        {
            var group = Data.Groups[e.Index];
            var firstOpponent = group.ExtractGroupMember(e.FirstOpponent);
            var secondOpponent = group.ExtractGroupMember(e.SecondOpponent);
            
            groupControl.SetTextBoxes(group.GetScore(firstOpponent, secondOpponent));
        }
        
        private void HandleOnGroupControlSubmitClicked(object sender, MatchSubmittedEventArgs e)
        {
            if (e.FirstOpponent.Equals(e.SecondOpponent))
            {
                MessageBox.Show(MatchBetweenOnePlayerMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            var group = Data.Groups[e.Index];
            Func<string[], Participant> matchOpponent = x => group.Members
                .FirstOrDefault(p => p.FirstName.Equals(x[0]) && p.Surname.Equals(x[1]));
            
            var firstOpponent = matchOpponent(e.FirstOpponent.Split(" "));
            var secondOpponent = matchOpponent(e.SecondOpponent.Split(" "));
            group.Matches[group.GetIndex(firstOpponent), group.GetIndex(secondOpponent)] = e.Score.first;
            group.Matches[group.GetIndex(secondOpponent), group.GetIndex(firstOpponent)] = e.Score.second;
        }
        
        
        private void HandleOnRoundSelected(object sender, CbItemSelectedEventArgs e)
        {
            if (!int.TryParse(e.ItemName, out var round))
            {
                throw new Exception();
            }
            var matches = Data.PlayOff.GetRoundMatches(round);
            playOffControl.SetMatches(matches);
        }
        
        private void HandleOnMatchSelected(object sender, PlayOffMatchSelectedEventArgs e)
        {
            if (!int.TryParse(e.ItemName, out var round))
            {
                throw new Exception();
            }

            var match = Data.PlayOff.GetMatch(round, e.SelectedMatch);

            playOffControl.SetMatch(match);
        }
        
        private void HandleOnSubmitPlayOffMatchClicked(object sender, MatchSubmittedEventArgs e)
        {
            var matchNode = Data.PlayOff.GetMatchNode(e.FirstOpponent, e.SecondOpponent, e.Index);
            matchNode.TreeMatch.Scores = e.Score;

            if (matchNode.IsRoot())
            {
                return;
            }
            
            var sibling = matchNode.GetSibling();
            if (sibling.GetScore() == default && !sibling.IsSolo())
            {
                return;
            }

            var nodeWinner = matchNode.GetWinner();
            var siblingWinner = sibling.GetWinner();
            matchNode.Successor.TreeMatch = new Match(
                new Tuple<Participant, Participant>(nodeWinner, siblingWinner), default);
        }

        #endregion
        
        #region HelperFunctions
        
        private Task<List<Participant>> ComputeAllGroupOrders()
        {
            return Task.Run(() =>
            {
                return Data.Groups.AsParallel().SelectMany(x => ComputeGroupOrder(x))
                    .OrderByDescending(x => x.Item2)
                    .ThenByDescending(x => x.Item3)
                    .Select(x => x.Item1)
                    .ToList();
            });

        }
        
        private List<Tuple<Participant, int, int>> ComputeGroupOrder(Group group)
        {
            return group.Members.Select(m =>
            {
                var memberCount = group.Members.Count;
                var index = group.GetIndex(m);
                var victories = 0;
                var points = 0;
                for (int i = 0; i < memberCount; i++)
                {
                    points += group.Matches[index, i];
                    if (group.Matches[index, i] > group.Matches[i, index])
                    {
                        victories++;
                    }
                }

                return new Tuple<Participant, int, int>(m, victories, points);
            }).ToList();
        }

        private void LoadParticipantList()
        {
            if (Data.Participants == null)
            {
              return;
            }
            
            foreach (var p in Data.Participants)
            {
                manageParticipantsControl.CbAddParticipant($"{p.FirstName} {p.Surname}");
            }
        }

        private void LoadGroups()
        {
            if (Data.Groups == null)
            {
                return;
            }
            
            groupControl.AddGroups(Data.Groups.Count);
        }

        private void LoadPlayOff()
        {
            if (Data.PlayOff == null)
            {
                return;
            }
            
            playOffControl.SetRounds(Data.PlayOff.Depth);
        }
        
        public void FixSuccessorTree()
        {
            if (Data.PlayOff == null)
            {
                return;
            }
            
            var root = Data.PlayOff.Root;

            root?.FixSuccessorTree();
        }

        private void RemoveParticipant(Participant participant)
        {
            Data.Participants.Remove(participant);
        }
        
        private void ShowParticipantInfo(Participant participant)
        {
            var showInfoForm = new ShowInfoForm(participant);
            showInfoForm.ShowDialog();
        }
        
        #endregion
    }
}