﻿using System;

namespace TournamentManager
{
    public class Match
    {
        public Tuple<Participant, Participant> Opponents { get; set; }
        public (int first, int second) Scores { get; set; }

        public Match()
        {
            Scores = default;
        }
        public Match(Tuple<Participant, Participant> opponents, (int first, int second) scores)
        {
            if (opponents.Item1.Equals(opponents.Item2))
            {
                throw new Exception();
            }

            Opponents = opponents;
            Scores = scores;
        }

        public Participant FirstOpponent()
        {
            return Opponents?.Item1;
        }

        public Participant SecondOpponent()
        {
            return Opponents?.Item2;
        }
    }
    
    
}