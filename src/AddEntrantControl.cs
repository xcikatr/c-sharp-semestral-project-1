﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TournamentManager
{
    public partial class AddEntrantControl : UserControl
    {
        public const string FirstNameError = "First Name cannot be empty";
        public const string SurnameError = "Surname cannot be empty";
        public const string SexError = "Sex has to be set";
        public const string BirthDateError = "Birth Date has to be set";
        public const string CategoryError = "Category cannot be empty";
        public const string ErrorCaption = "Error";

        public Participant NewParticipant { get; private set; }

        public delegate void ParticipantAddedDelegate(object sender, ParticipantAddedEventArgs e);
        
        public event ParticipantAddedDelegate SubmitParticipantClicked;

        protected void OnSubmitParticipantClicked(ParticipantAddedEventArgs e) =>
            SubmitParticipantClicked?.Invoke(this, e);
        
        public AddEntrantControl()
        {
            InitializeComponent();
            Func<int, int, IEnumerable<string> > createRange = (x, y) =>
                Enumerable.Range(x, y).Select(z => z.ToString());
            cbDateYear.Items.AddRange( (string[])createRange(DateTime.Today.Year -99 , 100 )
                .Reverse().ToArray() );
            cbDateMonth.Items.AddRange( (string[])createRange(1, 12).ToArray() );
            cbDateDay.Items.AddRange( (string[])createRange(1, 31).ToArray() );
            cbCategory.Items.AddRange( (string[])Enum.GetNames(typeof(AgeCategory)));
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ControlNotFilled())
                return;

            NewParticipant = new Participant()
            {
                FirstName = tbFirstName.Text,
                Surname = tbSurname.Text,
                Sex = rbSexMale.Checked ? SexType.Male : SexType.Female,
                BirthDate = new DateTime(int.Parse(cbDateYear.SelectedItem.ToString()!),
                                       int.Parse(cbDateMonth.SelectedItem.ToString()!),
                                         int.Parse(cbDateDay.SelectedItem.ToString()!)),
                Category = Enum.GetValues(typeof(AgeCategory)).Cast<AgeCategory>()
                    .First(x => x.ToString().Contains(cbCategory.SelectedItem.ToString() ?? "")),
                Club = tbClub.Text,
                LicenceNumber = tbLicenceNumber.Text,
                Nationality = tbNationality.Text,
                Rating = int.TryParse(tbRating.Text, out int rating) ? rating : 0
            };
            
            OnSubmitParticipantClicked(new ParticipantAddedEventArgs(NewParticipant));
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetControl();
        }

        private bool ControlNotFilled()
        {
            if (tbFirstName.Text.Equals(String.Empty))
            {
                MessageBox.Show(FirstNameError, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if (tbSurname.Text.Equals(String.Empty))
            {
                MessageBox.Show(SurnameError, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if (!rbSexMale.Checked && !rbSexFemale.Checked)
            {
                MessageBox.Show(SexError, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if (cbDateYear.SelectedItem == null || cbDateMonth.SelectedItem == null || cbDateDay.SelectedItem == null)
            {
                MessageBox.Show(BirthDateError, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if (cbCategory.SelectedIndex < 0)
            {
                MessageBox.Show(CategoryError, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            return false;
        }

        public void ResetControl()
        {
            tbClub.Text = String.Empty;
            tbNationality.Text = String.Empty;
            tbRating.Text = String.Empty;
            tbSurname.Text = String.Empty;
            tbFirstName.Text = String.Empty;
            tbLicenceNumber.Text = String.Empty;
            rbSexFemale.Checked = false;
            rbSexMale.Checked = false;
            cbDateYear.SelectedIndex = -1;
            cbDateMonth.SelectedIndex = -1;
            cbDateDay.SelectedIndex = -1;
            cbCategory.SelectedIndex = -1;
        }
    }
}