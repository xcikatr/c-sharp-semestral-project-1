﻿using System;
using System.IO;
using System.Windows.Forms;

namespace TournamentManager.src
{
    public partial class LoadFileForm : Form
    {
        private const string InvalidFilePathMessage = "Invalid file path";
        private const string ErrorCaption = "Error";
        public string PathToFile { get; private set; }
        private readonly string _relativeLocalPath;

        public LoadFileForm(string relativeLocalPath)
        {
            InitializeComponent();

            _relativeLocalPath = relativeLocalPath;
            importFileControl.LoadFileSelected += HandleLoadFileSelected;

            var directoryInfo = new DirectoryInfo(_relativeLocalPath);
            foreach (var file in directoryInfo.GetFiles("*.json"))
            {
                cbSavedTournaments.Items.Add(file.Name.Split('.')[0]);
            }
            btnLoad.Enabled = false;
        }

        private void HandleLoadFileSelected(object sender, EventArgs e)
        {
            if (importFileControl.SelectedPath.Length > 0 || cbSavedTournaments.SelectedIndex > -1)
            {
                btnLoad.Enabled = true;
                return;
            }
            btnLoad.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            if (cbSavedTournaments.SelectedIndex > -1)
            {
                PathToFile = Path.Combine(_relativeLocalPath + $"{cbSavedTournaments.SelectedItem}.json");
                return;
            }
            if (!File.Exists(importFileControl.SelectedPath))
            {
                DialogResult = DialogResult.Retry;
                MessageBox.Show(InvalidFilePathMessage, ErrorCaption,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            PathToFile = Path.Combine(importFileControl.SelectedPath);
        }

        private void cbSavedTournaments_SelectedIndexChanged(object sender, EventArgs e)
        {
            HandleLoadFileSelected(this, e);
        }
    }
}