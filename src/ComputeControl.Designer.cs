﻿
namespace TournamentManager.src
{
    partial class ComputeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGroups = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbGroupSize = new System.Windows.Forms.ComboBox();
            this.btnPlayOff = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbEliminate = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnGroups
            // 
            this.btnGroups.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnGroups.Location = new System.Drawing.Point(0, 0);
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Size = new System.Drawing.Size(200, 57);
            this.btnGroups.TabIndex = 0;
            this.btnGroups.Text = "Calculate Groups";
            this.btnGroups.UseVisualStyleBackColor = true;
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(0, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Max Size:";
            // 
            // cbGroupSize
            // 
            this.cbGroupSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGroupSize.FormattingEnabled = true;
            this.cbGroupSize.Location = new System.Drawing.Point(84, 60);
            this.cbGroupSize.Name = "cbGroupSize";
            this.cbGroupSize.Size = new System.Drawing.Size(80, 23);
            this.cbGroupSize.TabIndex = 2;
            // 
            // btnPlayOff
            // 
            this.btnPlayOff.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnPlayOff.Location = new System.Drawing.Point(290, 0);
            this.btnPlayOff.Name = "btnPlayOff";
            this.btnPlayOff.Size = new System.Drawing.Size(200, 57);
            this.btnPlayOff.TabIndex = 3;
            this.btnPlayOff.Text = "Calculate PlayOff";
            this.btnPlayOff.UseVisualStyleBackColor = true;
            this.btnPlayOff.Click += new System.EventHandler(this.btnPlayOff_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(286, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "% to eliminate:";
            // 
            // cbEliminate
            // 
            this.cbEliminate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEliminate.FormattingEnabled = true;
            this.cbEliminate.Location = new System.Drawing.Point(410, 60);
            this.cbEliminate.Name = "cbEliminate";
            this.cbEliminate.Size = new System.Drawing.Size(80, 23);
            this.cbEliminate.TabIndex = 5;
            // 
            // CalculateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbEliminate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPlayOff);
            this.Controls.Add(this.cbGroupSize);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGroups);
            this.Name = "ComputeControl";
            this.Size = new System.Drawing.Size(490, 92);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGroups;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbGroupSize;
        private System.Windows.Forms.Button btnPlayOff;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbEliminate;
    }
}
