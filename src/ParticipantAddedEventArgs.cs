﻿using System;

namespace TournamentManager
{
    public class ParticipantAddedEventArgs : EventArgs
    {
        public Participant Participant { get; }

        public ParticipantAddedEventArgs(Participant participant)
        {
            Participant = participant;
        }
    }
}