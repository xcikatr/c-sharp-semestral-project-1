﻿using System;
using Newtonsoft.Json;


namespace TournamentManager
{
    public class PlayOffTreeNode
    {
        public Match TreeMatch { get; set; }
        public PlayOffTreeNode PredecessorLeft { get; set; }
        public PlayOffTreeNode PredecessorRight { get; set; }
        
        [JsonIgnore]
        public PlayOffTreeNode Successor { get; private set; }

        [JsonConstructor]
        public PlayOffTreeNode(Match treeMatch, PlayOffTreeNode predecessorLeft, PlayOffTreeNode predecessorRight)
        {
            TreeMatch = treeMatch;
            PredecessorLeft = predecessorLeft;
            PredecessorRight = predecessorRight;
        }
        public PlayOffTreeNode( Match match, PlayOffTreeNode successor)
        {
            TreeMatch = match;
            Successor = successor;
        }
        
        public PlayOffTreeNode GetSibling()
        {
            if (Successor == null)
                return null;
            return Successor.PredecessorLeft == this ? Successor.PredecessorRight : Successor.PredecessorLeft;

        }

        public Participant GetFirstOpponent()
        {
            return TreeMatch.Opponents.Item1;
        }

        public Participant GetSecondOpponent()
        {
            return TreeMatch.Opponents.Item2;
        }

        public (int firstScore, int secondScore) GetScore()
        {
            return TreeMatch.Scores;
        }

        public void SetMatch(Tuple<Participant, Participant> opponents)
        {
            TreeMatch.Opponents = opponents;
        }

        public Participant GetWinner()
        {
            var firstOpponent = GetFirstOpponent();
            var secondOpponent = GetSecondOpponent();
            if (firstOpponent == null)
            {
                return secondOpponent;
            }

            if (secondOpponent == null)
            {
                return firstOpponent;
            }
            
            var score = GetScore();

            return score.firstScore > score.secondScore 
                ? firstOpponent 
                : secondOpponent;
        }

        public bool IsSolo()
        {
            return (GetFirstOpponent() == null && GetSecondOpponent() != null) ||
                   (GetFirstOpponent() != null && GetSecondOpponent() == null);
        }

        public bool IsRoot()
        {
            return Successor == null;
        }
        

        public void FixSuccessorTree()
        {
            if (PredecessorLeft != null)
            {
                PredecessorLeft.Successor = this;
                PredecessorLeft.FixSuccessorTree();
            }

            if (PredecessorRight != null)
            {
                PredecessorRight.Successor = this;
                PredecessorRight.FixSuccessorTree();
            }
        }
    }
}