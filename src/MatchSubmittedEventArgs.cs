﻿
namespace TournamentManager
{
    public class MatchSubmittedEventArgs : OpponentsSelectedEventArgs
    {
        public (int first, int second) Score { get; }

        public MatchSubmittedEventArgs(string firstOpponentName, string secondOpponentName,
            (int first, int second) score, int index) : 
            base(firstOpponentName, secondOpponentName, index)
        {
            Score = score;
        }
        
    }
}