﻿using System;

namespace TournamentManager
{
    public class CbItemSelectedEventArgs : EventArgs
    {
        public string ItemName { get; }

        public CbItemSelectedEventArgs(string itemName)
        {
            ItemName = itemName;
        }
    }
}