using System;
using System.Windows.Forms;

namespace TournamentManager.src
{
    static class Program
    {
        private const string RelativeLocalPath = "../../../Data/";
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var menuForm = new MenuForm(RelativeLocalPath);
            Application.Run(menuForm);
            if (menuForm.DialogResult != DialogResult.OK)
            {
                return;
            }
            var mainForm = new ManageTournamentForm(menuForm.TournamentPath);
            Application.Run(mainForm);
        }
    }
}