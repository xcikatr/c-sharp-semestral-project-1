﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TournamentManager
{
    [Serializable]
    public class Group
    {
        public List<Participant> Members { get; set; }
        public int[,] Matches { get; set; }

        public Group()
        {
            Members = new List<Participant>();
        }

        public Participant ExtractGroupMember(string name)
        {
            Participant result;
            if ((result = Members.FirstOrDefault(m =>
                name.Equals($"{m.FirstName} {m.Surname}"))) == null)
            {
                throw new Exception("this should not happen");
            }

            return result;


        }
        public int GetIndex(Participant opponent)
        {
            if (!Members.Contains(opponent))
                throw new Exception("this should not happen");
            return Members.FindIndex(x => x.Equals(opponent));
        }

        public (int firstIndex, int secondIndex) GetIndices(Participant firstOpponent, Participant secondOpponent)
        {
            return (firstIndex: GetIndex(firstOpponent), secondIndex: GetIndex(secondOpponent));
        }

        public (int first, int second) GetScore(Participant firstOpponent, Participant secondOpponent)
        {
            var indices = GetIndices(firstOpponent, secondOpponent);
            return (Matches[indices.firstIndex, indices.secondIndex],
                Matches[indices.secondIndex, indices.firstIndex]);
        }
    }
}