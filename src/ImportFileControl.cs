﻿using System;
using System.Windows.Forms;

namespace TournamentManager.src
{
    public partial class ImportFileControl : UserControl
    {
        private const string FilterOptions = "Json files (*.json)|*.json";
        
        public event EventHandler LoadFileSelected;
        
        protected void OnTbTextChanged(EventArgs e) => LoadFileSelected?.Invoke(this, e);

        public string SelectedPath
        {
            get => tbPath.Text;
            private set
            {
                tbPath.Text = value;
            }
        }
        
        public ImportFileControl()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FileDialog dialog = new OpenFileDialog();
            dialog.Filter = FilterOptions;
            if (dialog.ShowDialog(ParentForm) != DialogResult.OK)
            {
                return;
            }
            SelectedPath = dialog.FileName;
        }

        private void tbPath_TextChanged(object sender, EventArgs e)
        {
            OnTbTextChanged(e);
        }
    }
}