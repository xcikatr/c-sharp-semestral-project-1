﻿using System;
using System.IO;
using System.Windows.Forms;

namespace TournamentManager.src
{
    public partial class MenuForm : Form
    {
        private readonly string _relativeLocalPath;

        public string TournamentPath { get; private set; }
        public MenuForm(string relativeLocalPath)
        {
            InitializeComponent();
            _relativeLocalPath = relativeLocalPath;
        }

        private void btnCreateNew_Click(object sender, EventArgs e)
        {
            var fileNameForm = new FileNameForm(_relativeLocalPath);
            DialogResult dialogResult;
            while ((dialogResult = fileNameForm.ShowDialog(this)) != DialogResult.OK)
            {
                if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            TournamentPath = fileNameForm.PathToFile;
            File.Create(TournamentPath).Dispose();
            fileNameForm.Close();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var loadFileForm = new LoadFileForm(_relativeLocalPath);
            DialogResult dialogResult;
            while ((dialogResult = loadFileForm.ShowDialog(this)) != DialogResult.OK)
            {
                if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            TournamentPath = loadFileForm.PathToFile;
            loadFileForm.Close();
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}