﻿namespace TournamentManager
{
    public class ComputeEventArgs
    {
        public int Amount { get; }

        public ComputeEventArgs(int amount)
        {
            Amount = amount;
        }
    }
}