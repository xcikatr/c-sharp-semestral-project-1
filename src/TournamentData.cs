﻿using System;
using System.Collections.Generic;

namespace TournamentManager
{
    [Serializable]
    public class TournamentData
    {
        public List<Participant> Participants { get; set; }
        public List<Group> Groups { get; set; }
        public PlayOffTree PlayOff { get; set; }

        public TournamentData()
        {
            Participants = new List<Participant>();
        }
    }
}