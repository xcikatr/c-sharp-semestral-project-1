﻿using System;

namespace TournamentManager
{
    public class OpponentsSelectedEventArgs : EventArgs
    {
        public string FirstOpponent { get; }
        public string SecondOpponent { get; }
        
        public int Index { get; }

        public OpponentsSelectedEventArgs(string firstOpponentName, string secondOpponentName, int index)
        {
            FirstOpponent = firstOpponentName;
            SecondOpponent = secondOpponentName;
            Index = index;
        }

    }
}