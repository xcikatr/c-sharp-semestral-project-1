﻿
namespace TournamentManager.src
{
    partial class GroupControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbGroup = new System.Windows.Forms.ComboBox();
            this.cbFirstOpponent = new System.Windows.Forms.ComboBox();
            this.cbSecondOpponent = new System.Windows.Forms.ComboBox();
            this.tbSecondOpponentScore = new System.Windows.Forms.TextBox();
            this.tbFirstOpponentScore = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbGroup
            // 
            this.cbGroup.FormattingEnabled = true;
            this.cbGroup.Location = new System.Drawing.Point(62, 9);
            this.cbGroup.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbGroup.Name = "cbGroup";
            this.cbGroup.Size = new System.Drawing.Size(149, 23);
            this.cbGroup.TabIndex = 0;
            this.cbGroup.SelectedIndexChanged += new System.EventHandler(this.cbGroup_SelectedIndexChanged);
            // 
            // cbFirstOpponent
            // 
            this.cbFirstOpponent.FormattingEnabled = true;
            this.cbFirstOpponent.Location = new System.Drawing.Point(0, 48);
            this.cbFirstOpponent.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbFirstOpponent.Name = "cbFirstOpponent";
            this.cbFirstOpponent.Size = new System.Drawing.Size(134, 23);
            this.cbFirstOpponent.TabIndex = 1;
            this.cbFirstOpponent.SelectedIndexChanged += new System.EventHandler(this.cbFirstOpponent_SelectedIndexChanged);
            // 
            // cbSecondOpponent
            // 
            this.cbSecondOpponent.FormattingEnabled = true;
            this.cbSecondOpponent.Location = new System.Drawing.Point(354, 48);
            this.cbSecondOpponent.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbSecondOpponent.Name = "cbSecondOpponent";
            this.cbSecondOpponent.Size = new System.Drawing.Size(137, 23);
            this.cbSecondOpponent.TabIndex = 2;
            this.cbSecondOpponent.SelectedIndexChanged += new System.EventHandler(this.cbSecondOpponent_SelectedIndexChanged);
            // 
            // tbSecondOpponentScore
            // 
            this.tbSecondOpponentScore.Location = new System.Drawing.Point(285, 48);
            this.tbSecondOpponentScore.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbSecondOpponentScore.Name = "tbSecondOpponentScore";
            this.tbSecondOpponentScore.Size = new System.Drawing.Size(62, 23);
            this.tbSecondOpponentScore.TabIndex = 3;
            // 
            // tbFirstOpponentScore
            // 
            this.tbFirstOpponentScore.Location = new System.Drawing.Point(140, 48);
            this.tbFirstOpponentScore.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbFirstOpponentScore.Name = "tbFirstOpponentScore";
            this.tbFirstOpponentScore.Size = new System.Drawing.Size(62, 23);
            this.tbFirstOpponentScore.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(230, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "VS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(0, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Group:";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSubmit.Location = new System.Drawing.Point(343, 87);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(138, 33);
            this.btnSubmit.TabIndex = 7;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // GroupControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbFirstOpponentScore);
            this.Controls.Add(this.tbSecondOpponentScore);
            this.Controls.Add(this.cbSecondOpponent);
            this.Controls.Add(this.cbFirstOpponent);
            this.Controls.Add(this.cbGroup);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "GroupControl";
            this.Size = new System.Drawing.Size(493, 128);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbGroup;
        private System.Windows.Forms.ComboBox cbFirstOpponent;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox tbSecondOpponentScore;
        private System.Windows.Forms.TextBox tbFirstOpponentScore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbSecondOpponent;
        private System.Windows.Forms.Button btnSubmit;
    }
}
