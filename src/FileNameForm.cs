﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TournamentManager
{
    public partial class FileNameForm : Form
    {
        private const string ErrorCaption = "Error";
        private const string InvalidNameMessage = "Invalid name";
        private const string FileAlreadyExistsMessage = "Name is already used";
        
        public string PathToFile { get; private set; }

        private readonly string _relativeLocalPath;

        public FileNameForm(string relativeLocalPath)
        {
            InitializeComponent();
            _relativeLocalPath = relativeLocalPath;
        }
        
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (tbInputName.Text.Length < 1)
            {
                DialogResult = DialogResult.Retry;
                MessageBox.Show(InvalidNameMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            var directoryInfo = new DirectoryInfo(_relativeLocalPath);

            var fileName = $"{tbInputName.Text}.json";
            
            if (directoryInfo.GetFiles().Any(x => x.Name.Equals(fileName)))
            {
                DialogResult = DialogResult.Retry;
                MessageBox.Show(FileAlreadyExistsMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            PathToFile = Path.Combine(_relativeLocalPath + fileName) ;
            DialogResult = DialogResult.OK;
        }


    }
}