﻿using System.ComponentModel;

namespace TournamentManager
{
    partial class ManageTournamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.enterantCntrl = new TournamentManager.AddEntrantControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.manageParticipantsControl = new TournamentManager.src.ManageParticipantsControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupControl = new TournamentManager.src.GroupControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.computeControl = new TournamentManager.src.ComputeControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.playOffControl = new TournamentManager.src.PlayOffControl();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.enterantCntrl);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 424);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Participant";
            // 
            // enterantCntrl
            // 
            this.enterantCntrl.Location = new System.Drawing.Point(3, 22);
            this.enterantCntrl.Name = "enterantCntrl";
            this.enterantCntrl.Size = new System.Drawing.Size(342, 393);
            this.enterantCntrl.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.manageParticipantsControl);
            this.groupBox2.Location = new System.Drawing.Point(12, 442);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(351, 113);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Manage Participants";
            // 
            // manageParticipantsControl
            // 
            this.manageParticipantsControl.Location = new System.Drawing.Point(3, 19);
            this.manageParticipantsControl.Name = "manageParticipantsControl";
            this.manageParticipantsControl.Size = new System.Drawing.Size(342, 88);
            this.manageParticipantsControl.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupControl);
            this.groupBox3.Location = new System.Drawing.Point(394, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(502, 154);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Manage Group";
            // 
            // groupControl
            // 
            this.groupControl.Location = new System.Drawing.Point(6, 22);
            this.groupControl.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupControl.Name = "groupControl";
            this.groupControl.Size = new System.Drawing.Size(491, 123);
            this.groupControl.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.computeControl);
            this.groupBox4.Location = new System.Drawing.Point(394, 417);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(502, 139);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Compute";
            // 
            // computeControl
            // 
            this.computeControl.Location = new System.Drawing.Point(6, 22);
            this.computeControl.Name = "computeControl";
            this.computeControl.Size = new System.Drawing.Size(491, 111);
            this.computeControl.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.playOffControl);
            this.groupBox5.Location = new System.Drawing.Point(394, 207);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(502, 172);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Manage Play Off";
            // 
            // playOffControl
            // 
            this.playOffControl.Location = new System.Drawing.Point(4, 22);
            this.playOffControl.Name = "playOffControl";
            this.playOffControl.Size = new System.Drawing.Size(493, 139);
            this.playOffControl.TabIndex = 0;
            // 
            // ManageTournamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 568);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ManageTournamentForm";
            this.Text = "ManageTournamentForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private AddEntrantControl enterantCntrl;
        private System.Windows.Forms.GroupBox groupBox2;
        private src.ManageParticipantsControl manageParticipantsControl;
        private System.Windows.Forms.GroupBox groupBox3;
        private src.GroupControl groupControl;
        private System.Windows.Forms.GroupBox groupBox4;
        private src.ComputeControl computeControl;
        private System.Windows.Forms.GroupBox groupBox5;
        private src.PlayOffControl _playOffControl1;
        private src.PlayOffControl playOffControl;
    }
}