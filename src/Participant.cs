﻿using System;

namespace TournamentManager
{
    [Serializable]
    public class Participant
    {
        public string FirstName;
        public string Surname;
        public SexType Sex; 
        public int Rating;
        public AgeCategory Category;
        public DateTime BirthDate;
        public string Nationality;
        public string Club;
        public string LicenceNumber;
    }
}