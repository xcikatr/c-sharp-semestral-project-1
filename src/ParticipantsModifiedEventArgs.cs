﻿using System;

namespace TournamentManager
{
    public class ParticipantsModifiedEventArgs : EventArgs
    {
        public OperationType Type { get; }
        
        public string ParticipantName { get; }

        public ParticipantsModifiedEventArgs(string participantName, OperationType type)
        {
            ParticipantName = participantName;
            Type = type;
        }
    }
}