﻿using System;
using System.Windows.Forms;


namespace TournamentManager.src
{
    public partial class GroupControl : UserControl
    {
        private const string InvalidScoreMessage = "Score is invalid. Value has to be a number.";
        private const string ErrorCaption = "Error";
        public delegate void CbGroupItemSelectedDelegate(object sender, CbItemSelectedEventArgs e);
        public delegate void OpponentItemsSelectedDelegate(object sender, OpponentsSelectedEventArgs e);
        public delegate void MatchSelectedDelegate(object sender, MatchSubmittedEventArgs e);


        public event CbGroupItemSelectedDelegate CbGroupItemSelected;
        public event OpponentItemsSelectedDelegate OpponentsSelected;
        public event MatchSelectedDelegate SubmitClicked;

        protected void OnGroupSelected(CbItemSelectedEventArgs e) => CbGroupItemSelected?.Invoke(this, e);
        protected void OnOpponentsSelected(OpponentsSelectedEventArgs e) => OpponentsSelected?.Invoke(this, e);
        protected void OnSubmitClicked(MatchSubmittedEventArgs e) => SubmitClicked?.Invoke(this, e);
        
        
        public GroupControl()
        {
            InitializeComponent();
        }

        public void AddGroups(int groupCount)
        {
            cbGroup.Items.Clear();
            for (var i = 0; i < groupCount; i++)
            {
                cbGroup.Items.Add($"Group {i + 1}");
            }
        }

        public void AddToFirstOpponentList(string[] itemNames)
        {
            cbFirstOpponent.Items.AddRange((string[])itemNames);
        }

        public void AddToSecondOpponentList(string[] itemNames)
        {
            cbSecondOpponent.Items.AddRange((string[])itemNames);
        }

        private void cbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearInputData();
            OnGroupSelected(new CbItemSelectedEventArgs(cbGroup.SelectedItem.ToString()));
        }

        private void cbFirstOpponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnOpponentsSelected();
        }

        private void cbSecondOpponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnOpponentsSelected();
        }
        
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (cbFirstOpponent.SelectedItem == null || cbSecondOpponent.SelectedItem == null)
            {
                return;
            }

            if (cbGroup.SelectedItem == null || !int.TryParse(cbGroup.SelectedItem.ToString()?.Split(" ")[1], out var index))
            {
                return;
            }
            
            if (!int.TryParse(tbFirstOpponentScore.Text, out var firstScore) ||
                !int.TryParse(tbSecondOpponentScore.Text, out var secondScore))
            {
                MessageBox.Show(InvalidScoreMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ClearTextBoxes();
                return;
            }
            
            OnSubmitClicked(new MatchSubmittedEventArgs(cbFirstOpponent.SelectedItem.ToString(),
                cbSecondOpponent.SelectedItem.ToString(), (firstScore, secondScore), index - 1));
        }
        
        private void OnOpponentsSelected()
        {
            ClearTextBoxes();
            if (cbFirstOpponent.SelectedItem == null || cbSecondOpponent.SelectedItem == null)
            {
                return;
            }

            if (cbGroup.SelectedItem == null || !int.TryParse(cbGroup.SelectedItem.ToString()?.Split(" ")[1], out var index))
            {
                return;
            }
            
            OnOpponentsSelected(new OpponentsSelectedEventArgs(cbFirstOpponent.SelectedItem.ToString(),
                cbSecondOpponent.SelectedItem.ToString(), index - 1));
        }

        public void ClearInputData()
        {
            ClearTextBoxes();
            ClearComboBox(ref cbFirstOpponent);
            ClearComboBox(ref cbSecondOpponent);
        }

        public void SetTextBoxes((int first, int second) scores)
        {
            tbFirstOpponentScore.Text = scores.first.ToString();
            tbSecondOpponentScore.Text = scores.second.ToString();
        }

        #region HelperFunctions

        private static void ClearComboBox(ref ComboBox comboBox)
        {
            comboBox.Items.Clear();
            comboBox.Text = String.Empty;
        }

        private void ClearTextBoxes()
        {
            tbFirstOpponentScore.Clear();
            tbSecondOpponentScore.Clear();
        }

        #endregion

    }
}
