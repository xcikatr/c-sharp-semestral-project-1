﻿namespace TournamentManager
{
    public class PlayOffMatchSelectedEventArgs : CbItemSelectedEventArgs
    {
        public string SelectedMatch { get; }
        
        public PlayOffMatchSelectedEventArgs(string itemName, string selectedMatch)
            : base(itemName)
        {
            SelectedMatch = selectedMatch;
        }
    }
}