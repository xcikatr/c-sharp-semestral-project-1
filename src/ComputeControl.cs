﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TournamentManager.src
{
    public partial class ComputeControl : UserControl
    {
        private const string ErrorCaption = "Error";
        private const string GroupSizeNotSelectedMessage = "Max group size has to be selected to compute groups";

        private const string PlayOffEliminateNotSelectedMessage =
            "% to eliminate needs to be selected to proceed with play off computation";
        
        public event ComputeEventArgsDelegate ComputeGroups;
        public event ComputeEventArgsDelegate ComputePlayOff;

        public delegate void ComputeEventArgsDelegate(object sender, ComputeEventArgs e);
        protected void OnComputeGroups(ComputeEventArgs args) => ComputeGroups?.Invoke(this, args);
        protected void OnComputePlayOff(ComputeEventArgs args) => ComputePlayOff?.Invoke(this, args);

        
        public ComputeControl()
        {
            InitializeComponent();
            Func<int, int, int, IEnumerable<string> > createRange = (x, y, z) =>
                Enumerable.Range(x, y).Select(n => (n*z).ToString());
            cbGroupSize.Items.AddRange( (string[])createRange(4, 4, 1)
                .ToArray() );
            cbEliminate.Items.AddRange( (string[])createRange(0, 5, 10)
                .ToArray() );
        }

        private void btnGroups_Click(object sender, EventArgs e)
        {
            if (cbGroupSize.SelectedItem == null)
            {
                MessageBox.Show(GroupSizeNotSelectedMessage, ErrorCaption,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            OnComputeGroups(new ComputeEventArgs(int.Parse(cbGroupSize.SelectedItem.ToString()!)));
        }

        private void btnPlayOff_Click(object sender, EventArgs e)
        {
            if (cbEliminate.SelectedItem == null)
            {
                MessageBox.Show(PlayOffEliminateNotSelectedMessage, ErrorCaption,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            OnComputePlayOff(new ComputeEventArgs(int.Parse(cbEliminate.SelectedItem.ToString()!)));
        }
    }
}
