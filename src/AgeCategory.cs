﻿namespace TournamentManager
{
    public enum AgeCategory
    {
        Veteran,
        Junior,
        Cadet,
        Youth_14,
        Youth_12,
        Youth_10
    }
}