﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace TournamentManager
{
    public class PlayOffTree
    {
        public PlayOffTreeNode Root { get; }
        public int Depth { get; private set; }
        
        [JsonConstructor]
        public PlayOffTree(PlayOffTreeNode root, int depth)
        {
            Root = root;
            Depth = depth;
        }
        
        public PlayOffTree( List<Participant> participants)
        {
            Root = ComputeTree(participants);
        }

        public string[] GetRoundMatches(int round)
        {
            if (round > Depth || round < 1)
            {
                throw new Exception("If this happened you're just dumb");
            }

            var finalDepth = CalculateFinalDepth(round);
            var result = new List<string>();
            GetMatchNamesInDepth(Root, finalDepth, ref result);
            return result.Where(x => x.Length > 1)
                .Select(x =>
                {
                    var names = x.Split("_");
                    var resultName = "";
                    
                    if (names[0].Length > 0)
                        resultName = names[0];
                    
                    if (names[1].Length > 0)
                    {
                        if (resultName.Length > 0)
                            resultName += $" : {names[1]}";
                        else
                            resultName = names[1];
                    }

                    return resultName;
                }).ToArray();
        }

        public Match GetMatch(int round, string name)
        {
            if (round > Depth || round < 1)
            {
                throw new Exception("If this happened you're just dumb");
            }

            var finalDepth = CalculateFinalDepth(round);
            Match result = null;
            GetMatchInDepth(Root, finalDepth, ref result, name.Split(" : "));
            return result;
        }

        private void GetMatchNamesInDepth(PlayOffTreeNode node, int depth, ref List<string> result)
        {
            switch (depth)
            {
                case 1:
                    result.Add($"{node.TreeMatch.Opponents?.Item1?.Surname}_" +
                               $"{node.TreeMatch.Opponents?.Item2?.Surname}");
                    break;
                
                default:
                    GetMatchNamesInDepth(node.PredecessorLeft, depth - 1, ref result);
                    GetMatchNamesInDepth(node.PredecessorRight, depth - 1, ref result);
                    break;
            }
        }

        private void GetMatchInDepth(PlayOffTreeNode node, int depth, ref Match result, string[] names)
        {
            switch (depth)
            {
                case 1:
                    if (node.TreeMatch.Opponents == null)
                    {
                        break;
                    }

                    if (names.Length == 1)
                    {
                        if (node.GetFirstOpponent().Surname.Equals(names[0]) &&
                            node.GetSecondOpponent() == null)
                        {
                            result = node.TreeMatch;
                        }
                        break;
                    }
                    
                    if (node.GetFirstOpponent().Surname.Equals(names[0]) &&
                        node.GetSecondOpponent().Surname.Equals(names[1]))
                    {
                        result = node.TreeMatch;
                    }
                    
                    break;
                default:
                    GetMatchInDepth(node.PredecessorLeft, depth - 1, ref result, names);
                    GetMatchInDepth(node.PredecessorRight, depth - 1, ref result, names);
                    break;
            }
        }

        private PlayOffTreeNode ComputeTree(List<Participant> participants)
        {
            var depth = CalculateTreeDepth(participants.Count);
            Depth = depth;
            
            var unevenHalf = (int)Math.Pow(2, depth - 1);
            
            var participantsUpper = participants.Take(unevenHalf).ToList();
            var participantsLower = participants.Skip(unevenHalf).ToList();
            
            return ComputeTreeStep(ref participantsUpper, ref participantsLower, null, depth);
        }

        private PlayOffTreeNode ComputeTreeStep(ref List<Participant> participantsUpper,
            ref List<Participant> participantsLower, PlayOffTreeNode successor, int depth)
        {
            var match = new Match();
            var node = new PlayOffTreeNode(match, successor);
            switch (depth)
            {
                case 1:
                {
                    var upper = participantsUpper.Last();
                    var lower = participantsLower.FirstOrDefault();
                    match.Opponents = new Tuple<Participant, Participant>(upper, lower);
                    participantsUpper.Remove(upper);
                    participantsLower.Remove(lower);
                    
                    var sibling = node.GetSibling();
                    if (sibling == null)
                    {
                        break;
                    }
                    if (node.GetSecondOpponent() == null && sibling.GetSecondOpponent() == null)
                    {
                        successor.SetMatch(new Tuple<Participant, Participant>(
                            sibling.GetFirstOpponent(), node.GetFirstOpponent()));
                    }
                    break;
                }
                default:
                {
                    node.PredecessorLeft = ComputeTreeStep(ref participantsUpper, ref participantsLower, node, depth - 1);
                    node.PredecessorRight = ComputeTreeStep(ref participantsUpper, ref participantsLower, node, depth - 1);
                    break;
                }
            }

            return node;
        }

        private int CalculateTreeDepth(int minSize)
        {
            var size = 1;
            while (size < minSize)
            {
                size *= 2;
            }

            return (int)Math.Log2(size);
        }

        private int CalculateFinalDepth(int round)
        {
            return Depth - round + 1;
        }

        public PlayOffTreeNode GetMatchNode(string firstOpponent, string secondOpponent, int index)
        {
            if (index < 1 || index > Depth)
            {
                throw new Exception();
            }

            var finalDepth = CalculateFinalDepth(index);
            PlayOffTreeNode result = new PlayOffTreeNode(default, default);
            GetMatchNodeInDepth(Root, firstOpponent, secondOpponent, finalDepth, ref result);
            return result;
        }

        private void GetMatchNodeInDepth(PlayOffTreeNode node, string firstOpponent, string secondOpponent,
            int depth, ref PlayOffTreeNode result)
        {
            switch (depth)
            {
                case 1:
                    if (node.GetFirstOpponent() == null || node.GetSecondOpponent() == null)
                    {
                        break;
                    }
                    if (node.GetFirstOpponent().Surname == firstOpponent &&
                        node.GetSecondOpponent().Surname == secondOpponent)
                    {
                        result = node;
                    }
                    break;
                
                default:
                    GetMatchNodeInDepth(node.PredecessorLeft, firstOpponent, secondOpponent,
                        depth - 1, ref result);
                    GetMatchNodeInDepth(node.PredecessorRight, firstOpponent, secondOpponent,
                        depth - 1, ref result);
                    break;
            }
        }
    }
}