﻿using System.ComponentModel;

namespace TournamentManager
{
    partial class AddEntrantControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.tbSurname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbSexMale = new System.Windows.Forms.RadioButton();
            this.tbRating = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rbSexFemale = new System.Windows.Forms.RadioButton();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.tbNationality = new System.Windows.Forms.TextBox();
            this.tbClub = new System.Windows.Forms.TextBox();
            this.tbLicenceNumber = new System.Windows.Forms.TextBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.cbDateDay = new System.Windows.Forms.ComboBox();
            this.cbDateMonth = new System.Windows.Forms.ComboBox();
            this.cbDateYear = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(53, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name:";
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(151, 6);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(182, 23);
            this.tbFirstName.TabIndex = 2;
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(151, 44);
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(182, 23);
            this.tbSurname.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(67, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Surname:";
            // 
            // rbSexMale
            // 
            this.rbSexMale.AutoSize = true;
            this.rbSexMale.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rbSexMale.Location = new System.Drawing.Point(151, 82);
            this.rbSexMale.Name = "rbSexMale";
            this.rbSexMale.Size = new System.Drawing.Size(58, 23);
            this.rbSexMale.TabIndex = 5;
            this.rbSexMale.TabStop = true;
            this.rbSexMale.Text = "Male";
            this.rbSexMale.UseVisualStyleBackColor = true;
            // 
            // tbRating
            // 
            this.tbRating.Location = new System.Drawing.Point(151, 118);
            this.tbRating.Name = "tbRating";
            this.tbRating.Size = new System.Drawing.Size(182, 23);
            this.tbRating.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(105, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 21);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sex:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(84, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 21);
            this.label4.TabIndex = 9;
            this.label4.Text = "Rating:";
            // 
            // rbSexFemale
            // 
            this.rbSexFemale.AutoSize = true;
            this.rbSexFemale.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rbSexFemale.Location = new System.Drawing.Point(239, 82);
            this.rbSexFemale.Name = "rbSexFemale";
            this.rbSexFemale.Size = new System.Drawing.Size(71, 23);
            this.rbSexFemale.TabIndex = 10;
            this.rbSexFemale.TabStop = true;
            this.rbSexFemale.Text = "Female";
            this.rbSexFemale.UseVisualStyleBackColor = true;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSubmit.Location = new System.Drawing.Point(11, 351);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(132, 31);
            this.btnSubmit.TabIndex = 11;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(29, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Age Category:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(58, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "Birth Date:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(52, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 21);
            this.label7.TabIndex = 14;
            this.label7.Text = "Nationality:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(98, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 21);
            this.label8.TabIndex = 15;
            this.label8.Text = "Club:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(11, 310);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 21);
            this.label9.TabIndex = 16;
            this.label9.Text = "Licence Number:";
            // 
            // cbCategory
            // 
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(151, 156);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(182, 23);
            this.cbCategory.TabIndex = 17;
            // 
            // tbNationality
            // 
            this.tbNationality.Location = new System.Drawing.Point(151, 232);
            this.tbNationality.Name = "tbNationality";
            this.tbNationality.Size = new System.Drawing.Size(182, 23);
            this.tbNationality.TabIndex = 19;
            // 
            // tbClub
            // 
            this.tbClub.Location = new System.Drawing.Point(151, 270);
            this.tbClub.Name = "tbClub";
            this.tbClub.Size = new System.Drawing.Size(182, 23);
            this.tbClub.TabIndex = 20;
            // 
            // tbLicenceNumber
            // 
            this.tbLicenceNumber.Location = new System.Drawing.Point(151, 308);
            this.tbLicenceNumber.Name = "tbLicenceNumber";
            this.tbLicenceNumber.Size = new System.Drawing.Size(182, 23);
            this.tbLicenceNumber.TabIndex = 21;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnReset.Location = new System.Drawing.Point(201, 351);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(132, 31);
            this.btnReset.TabIndex = 22;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cbDateDay
            // 
            this.cbDateDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDateDay.FormattingEnabled = true;
            this.cbDateDay.IntegralHeight = false;
            this.cbDateDay.Location = new System.Drawing.Point(155, 194);
            this.cbDateDay.MaxDropDownItems = 12;
            this.cbDateDay.Name = "cbDateDay";
            this.cbDateDay.Size = new System.Drawing.Size(45, 23);
            this.cbDateDay.TabIndex = 23;
            // 
            // cbDateMonth
            // 
            this.cbDateMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDateMonth.FormattingEnabled = true;
            this.cbDateMonth.IntegralHeight = false;
            this.cbDateMonth.Location = new System.Drawing.Point(206, 194);
            this.cbDateMonth.MaxDropDownItems = 12;
            this.cbDateMonth.Name = "cbDateMonth";
            this.cbDateMonth.Size = new System.Drawing.Size(45, 23);
            this.cbDateMonth.TabIndex = 24;
            // 
            // cbDateYear
            // 
            this.cbDateYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDateYear.FormattingEnabled = true;
            this.cbDateYear.IntegralHeight = false;
            this.cbDateYear.Location = new System.Drawing.Point(257, 194);
            this.cbDateYear.MaxDropDownItems = 12;
            this.cbDateYear.Name = "cbDateYear";
            this.cbDateYear.Size = new System.Drawing.Size(76, 23);
            this.cbDateYear.TabIndex = 25;
            // 
            // AddEntrantControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbDateYear);
            this.Controls.Add(this.cbDateMonth);
            this.Controls.Add(this.cbDateDay);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.tbLicenceNumber);
            this.Controls.Add(this.tbClub);
            this.Controls.Add(this.tbNationality);
            this.Controls.Add(this.cbCategory);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.rbSexFemale);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbRating);
            this.Controls.Add(this.rbSexMale);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbSurname);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.label1);
            this.Name = "AddEntrantControl";
            this.Size = new System.Drawing.Size(345, 392);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.TextBox tbSurname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbSexMale;
        private System.Windows.Forms.TextBox tbRating;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbSexFemale;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.TextBox tbNationality;
        private System.Windows.Forms.TextBox tbClub;
        private System.Windows.Forms.TextBox tbLicenceNumber;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cbDateDay;
        private System.Windows.Forms.ComboBox cbDateMonth;
        private System.Windows.Forms.ComboBox cbDateYear;
    }
}