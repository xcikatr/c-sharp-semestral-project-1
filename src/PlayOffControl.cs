﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TournamentManager.src
{
    public partial class PlayOffControl : UserControl
    {
        private const string InvalidScoreMessage = "Score is invalid. Value has to be a number.";
        private const string ErrorCaption = "Error";
        public delegate void CbItemSelectedDelegate(object sender, CbItemSelectedEventArgs e);

        public delegate void PlayOffMatchSelectedDelegate(object sender, PlayOffMatchSelectedEventArgs e);

        public delegate void MatchSubmittedDelegate(object sender, MatchSubmittedEventArgs e);

        public event CbItemSelectedDelegate RoundSelected;
        public event PlayOffMatchSelectedDelegate MatchSelected;
        public event MatchSubmittedDelegate SubmitPlayOffMatchClicked;

        protected void OnRoundSelected(CbItemSelectedEventArgs e) => RoundSelected?.Invoke(this, e);
        protected void OnMatchSelected(PlayOffMatchSelectedEventArgs e) => MatchSelected?.Invoke(this, e);
        protected void OnMatchSubmitted(MatchSubmittedEventArgs e) => SubmitPlayOffMatchClicked?.Invoke(this, e);

        private const string NoOpponent = "Empty";
        private const string DefaultScore = "0";
        private const string PlaceHolderFirst = "FirstOpponent";
        private const string PlaceHolderSecond = "SecontOpponent";
        
        public PlayOffControl()
        {
            InitializeComponent();
        }

        private void cbRound_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetInput();
            if (cbRound.SelectedItem == null)
            {
                return;
            }
            OnRoundSelected(new CbItemSelectedEventArgs(cbRound.SelectedItem.ToString()));
        }

        private void cbMatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbRound.SelectedItem == null || cbMatch.SelectedItem == null)
            {
                return;
            }
            OnMatchSelected(new PlayOffMatchSelectedEventArgs(
                cbRound.SelectedItem.ToString(), cbMatch.SelectedItem.ToString()));
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (cbRound == null || cbMatch.SelectedItem == null)
            {
                return;
            }

            if (!int.TryParse(tbFirst.Text, out var scoreFirst)||
                !int.TryParse(tbSecond.Text, out var scoreSecond))
            {
                MessageBox.Show(InvalidScoreMessage, ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            var names = cbMatch.SelectedItem.ToString()?.Split(" : ");
            
            if (names?.Length != 2 || !int.TryParse(cbRound.SelectedItem.ToString(), out var round))
            {
                throw new Exception("This should never happen");
            }
            
            OnMatchSubmitted(new MatchSubmittedEventArgs(names[0], names[1],
                (scoreFirst, scoreSecond), round));
        }

        public void SetRounds(int amount)
        {
            cbRound.Items.Clear();
            cbRound.Text = String.Empty;
            cbRound.Items.AddRange((string[])Enumerable.Range(1, amount)
                .Select(x => x.ToString()).ToArray());
        }

        public void SetMatches(string[] matches)
        {
            cbMatch.Items.Clear();
            cbMatch.Text = String.Empty;
            cbMatch.Items.AddRange((string[])matches);
        }

        public void SetMatch(Match match)
        {
            lbFirst.Text = match.FirstOpponent() == null
                ? NoOpponent
                : match.FirstOpponent().Surname;

            lbSecond.Text = match.SecondOpponent() == null
                ? NoOpponent
                : match.SecondOpponent().Surname;

            tbFirst.Text = match.Scores == default
                ? DefaultScore
                : match.Scores.first.ToString();

            tbSecond.Text = match.Scores == default
                ? DefaultScore
                : match.Scores.second.ToString();
        }

        public void ResetInput()
        {
            lbFirst.Text = PlaceHolderFirst;
            lbSecond.Text = PlaceHolderSecond;
            tbFirst.Text = DefaultScore;
            tbSecond.Text = DefaultScore;
        }
    }
}
